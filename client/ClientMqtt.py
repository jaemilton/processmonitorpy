#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys, os, threading, datetime, json
import paho.mqtt.client as mqtt
import logging

nomeCliente = '';
topicoParaPublicar = '';
quantidadeMensagens = 100;

MENSAGEM_ERRO_PARAMETROS = "clientMqtt.py CLIENT_NAME TOPIC_TO_PUBLISH NUMBER_MESSAGES_TO_SEND";
diretorioLog = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "logs";
if not (os.path.exists(diretorioLog)):
    os.mkdir(diretorioLog);

if len(sys.argv) > 3:
    nomeCliente = sys.argv[1];
    #recupera o topico para publiar
    topicoParaPublicar = sys.argv[2];
    topicoParaAssinar = topicoParaPublicar + '_R';
    quantidadeMensagens = sys.argv[3];
else:
    raise Exception(MENSAGEM_ERRO_PARAMETROS);

# Mosquitto MQTT
# username 	= "admin"
# password 	= "12345"
hostname 	= "mqtt-server"
port 		= 8883

# Funções de Callback dos eventos
# on_connect = função chamada quando ocorre a conexão entre o cliente e o broker MQTT.
# on_message = função chamada quando uma mensagem de um tópico assinado for recebido.
# on_publish = função chamada quando uma mensagem for publicada. 
# on_subscribe = função chamada quando um tópico for assinado pelo cliente MQTT.
# on_log = função para debug do Paho.

def on_connect(client, userdata, flags, rc):
    print("rc: " + str(rc))

def on_message(client, obj, msg):
    # Imprime o conteudo da messagem.
    mensagem = json.loads(msg.payload.decode("utf-8"));
    mensagem["topicoResposta"] = msg.topic;
    mensagem["dataHoraResposta"] = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S");
    logging.debug(json.dumps(mensagem));
   
def on_publish(client, obj, mid):
    print("mid: " + str(mid))

def on_subscribe(client, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(client, obj, level, string):
    print(string)


def publicador(mqttClient, nomeCliente, topicosParaPublicar, qtdMensagens):
    for contador in range(1, int(qtdMensagens)):
        mensagem =  {
            "contador" : contador,
            "dataHoraEnvio": datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
            "nomeCliente": nomeCliente,
            "mensagem": "1234"
        };
        publicarMensagem(mqttClient, nomeCliente, topicosParaPublicar, contador, mensagem);

def publicarMensagem(mqttClient, nomeCliente, topicosParaPublicar, id_mensagem, mensagem):
    rc = mqttClient.publish(topicosParaPublicar, json.dumps(mensagem));
    if not (rc.rc == mqtt.MQTT_ERR_SUCCESS):
        print("Erro no envio da mensagem {0}".format(id_mensagem));


#------------------------------------------------

def main():
    logging.basicConfig(filename="{0}{1}{2}_{3}.log".format(diretorioLog, 
                                os.path.sep,
                                nomeCliente,
                                datetime.datetime.now().strftime("%Y%m%d%H%M%S")),
                        level=logging.DEBUG, 
                        #format='%(asctime)s;%(message)s',
                        format='%(message)s',
                        #datefmt='%d/%m/%Y %H:%M:%S',
                        filemode='w');
    
    mqttClient = mqtt.Client();

    # Assina as funções de callback
    mqttClient.on_message = on_message
    mqttClient.on_connect = on_connect
    mqttClient.on_publish = on_publish
    mqttClient.on_subscribe = on_subscribe

    # Uncomment to enable debug messages
    #mqttClient.on_log = on_log

    # Connecta ao Mosquitto MQTT. 
    # Informe o nome do usuário e senha. 
    # mqttClient.username_pw_set(username, password)

    # Carrega o certificado TLS.
    mqttClient.tls_set(os.path.dirname(os.path.abspath(__file__)) + os.path.sep + ".." + os.path.sep + "mqtt-ca.crt")

    # Informe o endereço IP ou DNS do servidor Mosquitto. 
    # Use 1883 para conexão SEM TLS, e 8883 para conexão segura TLS.
    mqttClient.connect(hostname, port);

    # Assina o tópico, com QoS level 0 (pode ser 0, 1 ou 2)
    # mqttClient.subscribe("#", 0)
    mqttClient.subscribe(topicoParaAssinar, 0);

    # Para publicar em um tópico, utilize a função a seguir.
    # mqttClient.publish(topic_publish, "25")

    theadPublicador = threading.Thread(target=publicador, 
                                        args=(mqttClient, 
                                                nomeCliente,
                                                topicoParaPublicar, 
                                                quantidadeMensagens
                                            )
                                    );
    theadPublicador.daemon = True;
    theadPublicador.start();

    # Permanece em loop mesmo se houver erros. 
    while True:
        try:
            rc = 0
            while rc == 0:
                rc = mqttClient.loop()
            print("rc: " + str(rc))
        except KeyboardInterrupt: 
            print("saindo");
            break;

if __name__ == '__main__':
    main();