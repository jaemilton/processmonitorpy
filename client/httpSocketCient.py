#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import socketio

# standard Python
sio = socketio.Client();

# asyncio
# sio = socketio.AsyncClient()

sio.connect('http://127.0.0.1:5000/', namespaces='/mensagem');

@sio.on('TOPICO_01_R', namespace='/mensagem')
def on_message(data):
    print('I received a message!')

sio.emit('TOPICO_01', 'teste', '/mensagem')