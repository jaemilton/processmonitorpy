#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os, sys
# sys.path.insert(0, "/Li");


"""
Importa a Classe LibProcessMonitor
"""
from .LibProcessMonitor import ProcessMonitor
pid = ProcessMonitor.getFirstPidByName("mosquitto");

if pid > 0:
    print("Mosquito esta rodando com pid: " + str(pid)); 
    diretorioLog = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "logs";
    if not (os.path.exists(diretorioLog)):
        os.mkdir(diretorioLog);

    print("Logs do monitoramento sendo gravavado no diretorio " + diretorioLog);
    pm = ProcessMonitor(pid, diretorioLog);
    print("Nome do processo: " + pm.getProcessName()); 

      
    done = False
    while not done:
        pm.theadExecuting.join(0.3)
        done = not pm.theadExecuting.is_alive()
    
    print("Processo de monitoramento terminou")

else:
     print("Mosquito nao esta rodando"); 
