#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os, sys
# sys.path.insert(0, "/Li");


"""
Importa a Classe LibProcessMonitor
"""
processName = 'apache2';

from LibProcessMonitor.LibProcessMonitor import ProcessMonitor
pidList = ProcessMonitor.getPidListByName(processName);

if (len(pidList) > 0):
    print(str(len(pidList)) + " process were found for the name: " + processName);
    for pid in pidList:
        print("Process running found pid: " + str(pid)); 

    diretorioLog = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "logs";

    if not (os.path.exists(diretorioLog)):
        os.mkdir(diretorioLog);

    print("Logs do monitoramento sendo gravado no diretorio " + diretorioLog);
    pm = ProcessMonitor(processIdList = pidList, logDirectory = diretorioLog);
    # print("Nome do processo: " + pm.getProcessName()); 

      
    done = False
    while not done:
        pm.theadExecuting.join(0.3)
        done = not pm.theadExecuting.is_alive()
    
    print("Processo de monitoramento terminou")

else:
     print("Apache nao esta rodando"); 
