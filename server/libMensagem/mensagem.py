#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import datetime, json

class Mensagem(object):

    def __init__(self, stringMensagemJson, origem):
        self.__stringMensagemJson = json.loads(stringMensagemJson.decode("utf-8"));
        self.__stringMensagemJson["origem"] = origem;
        self.__dataHoraMensagem = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S");

    def getJsonMensagem(self):
        return json.dumps(self.__stringMensagemJson);

    def gerarJsonMensagemResposta(self):
        mensagemResposta = self.__stringMensagemJson;
        mensagemResposta["dataHoraRecepcaoServer"] = self.__dataHoraMensagem;
        mensagemResposta["mensagemResposta"] = "Resposta da mensagem " + str(self.__stringMensagemJson['contador']);
        return json.dumps(mensagemResposta);