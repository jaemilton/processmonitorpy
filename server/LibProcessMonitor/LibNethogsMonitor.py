#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import ctypes
import signal
import datetime
import threading
import os

# Here are some definitions from libnethogs.h
# https://github.com/raboof/nethogs/blob/master/src/libnethogs.h
# Possible actions are NETHOGS_APP_ACTION_SET & NETHOGS_APP_ACTION_REMOVE
# Action REMOVE is sent when nethogs decides a connection or a process has died. There are two
# timeouts defined, PROCESSTIMEOUT (150 seconds) and CONNTIMEOUT (50 seconds). AFAICT, the latter
# trumps the former so we see a REMOVE action after ~45-50 seconds of inactivity.
class Action():
    SET = 1
    REMOVE = 2

    MAP = {SET: 'SET', REMOVE: 'REMOVE'}

class LoopStatus():
    """Return codes from nethogsmonitor_loop()"""
    OK = 0
    FAILURE = 1
    NO_DEVICE = 2

    MAP = {OK: 'OK', FAILURE: 'FAILURE', NO_DEVICE: 'NO_DEVICE'}

# The sent/received KB/sec values are averaged over 5 seconds; see PERIOD in nethogs.h.
# https://github.com/raboof/nethogs/blob/master/src/nethogs.h#L43
# sent_bytes and recv_bytes are a running total
class NethogsMonitorRecord(ctypes.Structure):
    """ctypes version of the struct of the same name from libnethogs.h"""
    _fields_ = (('record_id', ctypes.c_int),
                ('name', ctypes.c_char_p),
                ('pid', ctypes.c_int),
                ('uid', ctypes.c_uint32),
                ('device_name', ctypes.c_char_p),
                ('sent_bytes', ctypes.c_uint64),
                ('recv_bytes', ctypes.c_uint64),
                ('sent_kbs', ctypes.c_float),
                ('recv_kbs', ctypes.c_float),
                )




class LibNethogs(object):

    # This is a Python 3 demo of how to interact with the Nethogs library via Python. The Nethogs
    # library operates via a callback. The callback implemented here just formats the data it receives
    # and prints it to stdout. This must be run as root (`sudo python3 python-wrapper.py`).
    # By Philip Semanchuk (psemanchuk@caktusgroup.com) November 2016
    # Copyright waived; released into public domain as is.

    # The code is multi-threaded to allow it to respond to SIGTERM and SIGINT (Ctrl+C).  In single-
    # threaded mode, while waiting in the Nethogs monitor loop, this Python code won't receive Ctrl+C
    # until network activity occurs and the callback is executed. By using 2 threads, we can have the
    # main thread listen for SIGINT while the secondary thread is blocked in the monitor loop.

    #######################
    # BEGIN CONFIGURATION #
    #######################

   
    LIBRARY_PATH = 'libnethogs'

    # LIBRARY_NAME has to be exact, although it doesn't need to include the full path.
    # The version tagged as 0.8.5 (download link below) builds a library with this name.
    # https://github.com/raboof/nethogs/archive/v0.8.5.tar.gz
    LIBRARY_NAME = 'libnethogs.so.0.8.5-61-gdbbee09'

    # EXPERIMENTAL: Optionally, specify a capture filter in pcap format (same as
    # used by tcpdump(1)) or None. See `man pcap-filter` for full information.
    # Note that this feature is EXPERIMENTAL (in libnethogs) and may be removed or
    # changed in an incompatible way in a future release.
    # example:
    # FILTER = 'port 80 or port 8080 or port 443'
    FILTER = None

    #####################
    # END CONFIGURATION #
    #####################

    # You can use this to monitor only certain devices, like:
    # device_names = ['enp4s0', 'docker0']
    def __init__(self, pidList, dataUpdateCallback, device_names = [], startMonitoring=True):
        if (dataUpdateCallback == None):
            raise('dataUpdateCallBack is mandatory');

        self.__dataUpdateCallback = dataUpdateCallback;
        self.device_names = device_names;
        self.executing = False;
        self.__lastDatetimeUpdate = datetime.datetime.now();
        self.pidList = pidList;
        # self.__record_id = 0;
        # self.__sent_bytes_acumulado = 0; 
        # self.__recv_bytes_acumulado = 0;

        # self.__sent_bytes = 0; 
        # self.__recv_bytes = 0;
        # self.__sent_kbs = 0;
        # self.__recv_kbs = 0;

        #lib = ctypes.CDLL(LIBRARY_NAME)
        libabspath = os.path.dirname(os.path.abspath(__file__)) \
                        + os.path.sep + LibNethogs.LIBRARY_PATH  \
                        + os.path.sep + LibNethogs.LIBRARY_NAME;


        self.__lib = ctypes.CDLL(libabspath);
        if (startMonitoring):
            self.startMonitoring();

    def startMonitoring(self):
        if (not self.executing):
            signal.signal(signal.SIGINT, self.__signal_handler);
            signal.signal(signal.SIGTERM, self.__signal_handler);
            
            self.monitor_thread = threading.Thread(
                target=self.__run_monitor_loop, args=()
            );
            self.monitor_thread.start();
            self.executing = True;

    def stopMonitoring(self):
        self.__lib.nethogsmonitor_breakloop();
      
    def __signal_handler(self, signal, frame):
        self.__lib.nethogsmonitor_breakloop();

    def __dev_args(self):
        """
        Return the appropriate ctypes arguments for a device name list, to pass
        to libnethogs ``nethogsmonitor_loop_devices``. The return value is a
        2-tuple of devc (``ctypes.c_int``) and devicenames (``ctypes.POINTER``)
        to an array of ``ctypes.c_char``).
        :param devnames: list of device names to monitor
        :type devnames: list
        :return: 2-tuple of devc, devicenames ctypes arguments
        :rtype: tuple
        """
        devc = len(self.device_names)
        devnames_type = ctypes.c_char_p * devc
        devnames_arg = devnames_type()
        for idx, val in enumerate(self.device_names):
            devnames_arg[idx] = (val + chr(0)).encode('ascii')
        return ctypes.c_int(devc), ctypes.cast(
            devnames_arg, ctypes.POINTER(ctypes.c_char_p)
        )


    def __run_monitor_loop(self):
        # Create a type for my callback func. The callback func returns void (None), and accepts as
        # params an int and a pointer to a NethogsMonitorRecord instance.
        # The params and return type of the callback function are mandated by nethogsmonitor_loop().
        # See libnethogs.h.
        CALLBACK_FUNC_TYPE = ctypes.CFUNCTYPE(
            ctypes.c_void_p, ctypes.c_int, ctypes.POINTER(NethogsMonitorRecord)
        )

        filter_arg = self.FILTER
        if filter_arg is not None:
            filter_arg = ctypes.c_char_p(filter_arg.encode('ascii'))

        if len(self.device_names) < 1:
            # monitor all devices
            rc = self.__lib.nethogsmonitor_loop(
                CALLBACK_FUNC_TYPE(self.__network_activity_callback),
                filter_arg
            )
        else:
            devc, devicenames = self.dev_args()
            rc = self.__lib.nethogsmonitor_loop_devices(
                CALLBACK_FUNC_TYPE(self.__network_activity_callback),
                filter_arg,
                devc,
                devicenames,
                ctypes.c_bool(False)
            )

        if rc != LoopStatus.OK:
            print('nethogsmonitor_loop returned {}'.format(LoopStatus.MAP[rc]))
        else:
            self.executing = False;
            print('exiting monitor loop')

    def __network_activity_callback(self, action, data):
        if data.contents.pid in self.pidList :
            # processInfo.lastDatetimeUpdate = datetime.datetime.now();
            #Update data
            # processInfo.updateNetData(data.contents.record_id,
            #     data.contents.sent_bytes,
            #     data.contents.recv_bytes,
            #     data.contents.sent_kbs,
            #     data.contents.recv_kbs);

            #send infos to callBack method
            self.__dataUpdateCallback(data.contents.pid, 
                        data.contents.record_id,
                        data.contents.sent_bytes,
                        data.contents.sent_kbs,
                        data.contents.recv_bytes,
                        data.contents.recv_kbs);
