#!/usr/bin/python3
# -*- coding: UTF-8 -*-x


import psutil, subprocess, threading, os, signal, time, sys, datetime
# sys.path.insert(0, "/projetos/flask-test");

from LibProcessMonitor.LibNethogsMonitor import LibNethogs
from LibProcessMonitor.ProcessInfo import  ProcessInfo

# Classe Pai
class ProcessMonitor(object):

    def __init__(self, processId, logDirectory, deviceNames=[], updateInterval=1, debug=False):
        self.__init__(
            processIdList = [processId], 
            logDirectory=logDirectory, 
            deviceNames=deviceNames, 
            updateInterval=updateInterval, 
            debug=debug
        )
        

    def __init__(self, processIdList, logDirectory, deviceNames=[], updateInterval=1, debug=False):
        self.__updateInterval = updateInterval;
        self.__processIdList = processIdList;
        self.__dictinaryProcessInfo = {};
        for processId in processIdList:
            self.__dictinaryProcessInfo[processId] = \
                ProcessInfo(pid = processId, logDirectory=logDirectory);
        
        self.__rxBytes = 0;
        self.__txBytes = 0;
        self.__txBytesPorSegundo = 0;
        self.__rxBytesPorSegundo = 0;
        self.__percentualCpu = 0.0;
        self.__bytesMemoriaRam = 0;
        self.__percentualMemoria = 0.0;
        self.setDebug(debug);
        self.netgogs = LibNethogs(pidList=processIdList, 
                    dataUpdateCallback=self.__updateDataCallback, 
                    device_names=deviceNames);
        self.theadExecuting = threading.Thread(target=self.__checkNethogsIsRunning, args=());
        self.theadExecuting.daemon = True;
        self.theadExecuting.start();
      
    def __checkNethogsIsRunning(self):
        while self.netgogs.executing:
            time.sleep(1);
        print("Finishing Process monitoring");


    def setDebug(self, debug):
        self.__debug = debug;
        if (self.__debug):
            threadDebug = threading.Thread(target=self.__printProcessStatistics, args=());
            threadDebug.daemon = True;                          # Daemonize thread
            threadDebug.start();

    def __updateDataCallback(self,
                        pid,
                        record_id, 
                        sent_bytes, 
                        sent_kbs, 
                        recv_bytes,
                        recv_kbs):
        
        if pid in self.__dictinaryProcessInfo :
            processInfo = self.__dictinaryProcessInfo[pid];
            processInfo.updateData(record_id,
                sent_bytes,
                recv_bytes,
                sent_kbs,
                recv_kbs);

    def checkIfProcessRunning(processName):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if processName.lower() in proc.name().lower():
                    return True
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return False;

    def getFirstPidByName(processName):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if processName.lower() in proc.name().lower():
                    return proc.pid;
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return 0;

    def getPidListByName(processName):
        '''
        Check if there is any running process that contains the given name processName.
        '''
        pidList = [];
        #Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if processName.lower() in proc.name().lower():
                    pidList.append(proc.pid);
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return pidList;


    def getProcessName(self):
        return self.__process.name();

    def __printProcessStatistics(self):
        while self.__debug:
            if (self.__isNethogsExecuting):
                print("CPU = " + str(self.__percentualCpu) + " %");
                print("Memoria: {0:d} Bytes, {1:.2f}".format(self.__bytesMemoriaRam, 
                                self.__percentualMemoria) + " %");
                print("Tx: " + str(self.__txBytes) + " Bytes, Taxa: " + str(self.__txBytesPorSegundo) + "KB/s");
                print("Rx:" + str(self.__rxBytes) + " Bytes, Taxa: " + str(self.__rxBytesPorSegundo) + "KB/s");
            else:
                print("Monitoramento finalizado");
            
            time.sleep(self.__intevaloAtualizacao);

    def getTxBytes(self):
        return self.__txBytes;

    def getRxBytes(self):
        return self.__rxBytes;