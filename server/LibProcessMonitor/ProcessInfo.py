import datetime, psutil, logging, os

class ProcessInfo(object):

    LOG_HEAD = 'data;sent_bytes;sent_kbs;recv_bytes;recv_kbs;cpu_per;men_per;men_bytes'

    def __init__(self,
                pid: int,
                logDirectory: str,
                lastDatetimeUpdate: datetime = datetime.datetime.now(),
                sent_bytes: int = 0,
                recv_bytes: int = 0,
                sent_kbs: int = 0,
                recv_kbs: int = 0,
                cpuPercentage: float = 0.0,
                ramMemoryBytes: int = 0,
                memoriyPercentage: float = 0.0):

        self.__process = psutil.Process(pid);
        self.pid = pid;
        if (logDirectory is None and logDirectory == ''):
            raise Exception("The log path must be informed");
        self.__logDirectory = logDirectory;
        self.lastDatetimeUpdate = lastDatetimeUpdate;
        self.sent_bytes = sent_bytes;
        self.recv_bytes = recv_bytes;
        self.sent_kbs = sent_kbs;
        self.recv_kbs = recv_kbs;
        self.cpuPercentage = cpuPercentage;
        self.ramMemoryBytes = ramMemoryBytes;
        self.ramMemoriyPercentage = memoriyPercentage;
        self.__record_id = 0;
        self.__accumulated_sent_bytes = 0;
        self.__accumulated_recv_bytes = 0;
        self.__writtenLog = False;
        self.__lastWrittenLog = '';
        self.logFileName = ProcessInfo.getLogFileName(self.__logDirectory, 
                                self.__process.name(), 
                                self.pid);
        self.logger = ProcessInfo.get_logger(self.__process.name(), 
                                    str(self.pid),
                                    self.logFileName);
        self.logger.debug(self.LOG_HEAD);

    
    def get_logger(processName, pid, log_file, logFormatter='%(message)s', level=logging.DEBUG):
        """Function setup as many loggers as you want"""

        formatter = logging.Formatter(logFormatter);
        handler = logging.FileHandler(log_file);
        handler.setFormatter(formatter);

        logger = logging.getLogger("{0}{1}".format(processName, pid));
        logger.setLevel(level);
        logger.addHandler(handler);

        return logger

    def getLogFileName(logDirectory, processName, pid):
        pathSeparator = '';
        if (not logDirectory.endswith(os.path.sep)):
            pathSeparator = os.path.sep;

        return "{0}{1}{2}_pid_{3}_{4}.csv".format(logDirectory, 
                                pathSeparator,
                                processName,
                                pid,
                                datetime.datetime.now().strftime('%Y%m%d%H%M%S'));
    
    def updateData(self, record_id, sent_bytes, recv_bytes, sent_kbs, recv_kbs):
        self.lastDatetimeUpdate = datetime.datetime.now();
        if (self.__record_id != record_id):
            self.__record_id  = record_id;
            self.__accumulated_sent_bytes = sent_bytes;
            self.__accumulated_recv_bytes = recv_bytes;
            
        self.sent_bytes = self.__accumulated_sent_bytes + sent_bytes; 
        self.recv_bytes = self.__accumulated_recv_bytes + recv_bytes;
        self.sent_kbs = sent_kbs;
        self.recv_kbs = recv_kbs; 
        self.cpuPercentage = self.__process.cpu_percent();
        self.ramMemoryBytes = self.__process.memory_info().vms;
        self.ramMemoriyPercentage = self.__process.memory_percent();
        self.__writeLog();

    def __getLogToWrite(self):
        logToWrite = None;
        # self.__atualizarEstatiticaProcesso();
        # CABECALHO_LOG = 'data;pid;sent_bytes;sent_kbs;recv_bytes;recv_kbs;cpu_per;men_per;men_bytes'
        logToWrite = "{0};{1:.2f};{2};{3:.2f};{4};{5};{6:.2f}".format(
            self.sent_bytes,             #1
            self.sent_kbs,   #2
            self.recv_bytes,             #3
            self.recv_kbs,   #4
            self.cpuPercentage,       #5
            self.ramMemoryBytes,     #6
            self.ramMemoriyPercentage);  #7

        if (self.__lastWrittenLog != logToWrite):
            self.__writtenLog = False;
            self.__lastWrittenLog = logToWrite;
        else:
            logToWrite = None;
        return logToWrite;

    def __writeLog(self):
        logToWrite = self.__getLogToWrite();
        if logToWrite is not None and not self.__writtenLog:
            self.logger.debug("{0};{1}".format(
                            datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 
                            logToWrite));
            self.__writtenLog = True;