#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#
# Conteudo do arquivo `myapp.py`
#
from flask import Flask
from flask import request
import os, sys

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + 
                os.path.sep + '..' + os.path.sep + '..' + os.path.sep + '..' +  os.path.sep +
                'libMensagem')

from mensagem import Mensagem
import json
app = Flask(__name__)


@app.route("/")
def hello():
    return "Servico no AR";

@app.route('/mensagem/TOPICO_01', methods=['POST']) #GET requests will be blocked
def json_example():
    mensagem =  Mensagem(request.data); 
    origem = request.path.split('/')[-1]; #Recupera a parte final da url
    mensagemResposta = mensagem.gerarMensagemResposta(origem);

    return mensagemResposta;

if __name__ == "__main__":
    app.run(host='0.0.0.0')