#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from flask import Flask, render_template
from flask_socketio import SocketIO, emit

app = Flask(__name__)
# app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('TOPICO_01', namespace='/mensagem')
def mensagem_topico_01(message):
    emit('TOPICO_01_R', {'data': message['data']})

@socketio.on('TOPICO_02', namespace='/mensagem')
def test_message(message):
    emit('TOPICO_02_R', {'data': message['data']}, broadcast=True)

@socketio.on('connect', namespace='/mensagem')
def test_connect():
    emit('my response', {'data': 'Connected'})

@socketio.on('disconnect', namespace='/mensagem')
def test_disconnect():
    print('Client disconnected')

if __name__ == '__main__':
    socketio.run(app)